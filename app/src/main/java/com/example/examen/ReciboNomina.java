package com.example.examen;

public class ReciboNomina {
    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtras, impuesto;


    public ReciboNomina(){
        this.numRecibo = (int) (Math.random() * 1000) + 1;
        this.puesto = 0;
        this.nombre = "";
        this.horasTrabNormal = 0;
        this.horasTrabExtras = 0;
        this.impuesto = (float) 0.16;

    }
    public ReciboNomina(int puesto, String nombre, float horasTrabNormal, float horasTrabExtras) {
        this.numRecibo = (int) (Math.random() * 1000) + 1;;
        this.puesto = puesto;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.impuesto = (float) 0.16;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }


    public float calcularSubtotal(){
        float horasNormales, horasExtras;

        float resultado = 0;

        switch (getPuesto()){
            case 1:
                horasNormales = getHorasTrabNormal()*50;
                horasExtras = getHorasTrabExtras()*100;
                resultado = horasExtras + horasNormales;
                break;

            case 2:
                horasNormales = getHorasTrabNormal()*70;
                horasExtras = getHorasTrabExtras()*140;
                resultado = horasExtras + horasNormales;
                break;

            case 3:
                horasNormales = getHorasTrabNormal()*100;
                horasExtras = getHorasTrabExtras()*200;
                resultado = horasExtras + horasNormales;
                break;
        }
        return resultado;
    }


    public float calcularImpuesto(){
        float resultado = 0;
        resultado = (float) (calcularSubtotal()*getImpuesto());

        return resultado;
    }

    public float calcularTotal(){
        float resultado = 0;
        resultado = calcularSubtotal() - calcularImpuesto();
        return resultado;
    }
}