package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class NominaActivity extends AppCompatActivity {

    ReciboNomina reciboNomina = new ReciboNomina();
    TextView txtNumRecibo, txtNombre, txtHorasTrabajadas, txtHorasExtras, txtSubtotal, txtImpuesto, txtTotal;
    Button btnCalcular, btnLimpiar, btnRegresar;

    Bundle datos;
    RadioButton radioAuxiliar, radioAlbañil, radioIng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nomina_activity);
        datos = getIntent().getExtras();
        reciboNomina.setNombre(datos.getString("nombre"));

        txtNumRecibo = (TextView) findViewById(R.id.txtNumRecibo);
        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtHorasTrabajadas = (TextView) findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = (TextView) findViewById(R.id.txtHorasExtras);
        txtSubtotal = (TextView) findViewById(R.id.txtSubtotal);
        txtImpuesto = (TextView) findViewById(R.id.txtImpuesto);
        txtTotal = (TextView) findViewById(R.id.txtTotalAPagar);

        radioAlbañil = (RadioButton) findViewById(R.id.radioAlbañil);
        radioAuxiliar = (RadioButton) findViewById(R.id.radioAuxiliar);
        radioIng = (RadioButton) findViewById(R.id.radioIngeniero);

        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtNumRecibo.setText(String.valueOf(reciboNomina.getNumRecibo()));
        txtNombre.setText(reciboNomina.getNombre());

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtHorasTrabajadas.getText().toString().isEmpty() || txtHorasExtras.getText().toString().isEmpty()){
                    Toast.makeText(NominaActivity.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }else{
                    if(radioAuxiliar.isChecked() == true){
                        reciboNomina.setPuesto(1);
                        reciboNomina.setHorasTrabNormal(Float.parseFloat(txtHorasTrabajadas.getText().toString()));
                        reciboNomina.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
                        txtSubtotal.setText("$" + String.valueOf(reciboNomina.calcularSubtotal()) + " MXN");
                        txtImpuesto.setText("$" + String.valueOf(reciboNomina.calcularImpuesto()) + " MXN");
                        txtTotal.setText("$" + String.valueOf(reciboNomina.calcularTotal()) + " MXN");

                    }else  if(radioAlbañil.isChecked() == true){
                        reciboNomina.setPuesto(2);
                        reciboNomina.setHorasTrabNormal(Float.parseFloat(txtHorasTrabajadas.getText().toString()));
                        reciboNomina.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
                        txtSubtotal.setText("$" + String.valueOf(reciboNomina.calcularSubtotal()) + " MXN");
                        txtImpuesto.setText("$" + String.valueOf(reciboNomina.calcularImpuesto()) + " MXN");
                        txtTotal.setText("$" + String.valueOf(reciboNomina.calcularTotal()) + " MXN");

                    }else if(radioIng.isChecked() == true){
                        reciboNomina.setPuesto(3);
                        reciboNomina.setHorasTrabNormal(Float.parseFloat(txtHorasTrabajadas.getText().toString()));
                        reciboNomina.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
                        txtSubtotal.setText("$" + String.valueOf(reciboNomina.calcularSubtotal()) + " MXN");
                        txtImpuesto.setText("$" + String.valueOf(reciboNomina.calcularImpuesto()) + " MXN");
                        txtTotal.setText("$" + String.valueOf(reciboNomina.calcularTotal()) + " MXN");
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasExtras.setText("");
                txtHorasTrabajadas.setText("");
                txtImpuesto.setText("");
                txtSubtotal.setText("");
                txtTotal.setText("");
                radioAuxiliar.setChecked(true);
                txtHorasTrabajadas.requestFocus();
                Toast.makeText(NominaActivity.this, "Listo", Toast.LENGTH_SHORT).show();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NominaActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
